Rails.application.routes.draw do




  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  scope module: :api do
    scope module: :v1 do
      resources :orders, :defaults => { :format => 'json' }, :only => [:index,:show] do
        post "" ,action: :direction
      end
      resources :notice, :defaults => { :format => 'json' }, :only => [:index]
      resource :users, :defaults => { :format => 'json' }, :only => [:update] do
        post :login
      end
    end
  end
  devise_for :staffs, path: 'auth', path_names: { sign_in: 'login', sign_out: 'logout', password: 'secret', confirmation: 'verification', unlock: 'unblock', registration: 'register', sign_up: 'cmon_let_me_in' }
  scope module: :staffs do
    resources :staff
  end

  root 'site#home'

  resources :task_job_links
  resources :vacations
  resources :divisions do
    collection do
      put :update_tree
    end
  end
  resources :facilities
  resources :calendar , :only => [:index]
  resources :companies
  resources :media_assets
  resources :task_performs
  resources :tasks
  resources :professions
  resources :contract_types
  resources :workers
  resources :year_shedules
  resources :consumables
  resources :job_links
  resources :appliances
  resources :inventories
  resources :jobs
  resources :job_types
  resources :job_units
  resources :notifications
  resources :work_time_limits
end
