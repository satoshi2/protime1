# Be sure to restart your server when you modify this file.

# Version of your assets, change this if you want to expire all your assets.
Rails.application.config.assets.version = '1.0'

# Add additional assets to the asset load path.
# Rails.application.config.assets.paths << Emoji.images_path
# Add Yarn node_modules folder to the asset load path.
Rails.application.config.assets.paths << Rails.root.join('node_modules')

# Precompile additional assets.
# application.js, application.css, and all non-JS/CSS in the app/assets
# folder are already added.
Rails.application.config.assets.precompile += %w(
bootkit/bootkit.css
bootkit/modernizr-custom.js
bootkit/bootkit.js
bootkit/jquery.nestable.js
bootkit/init-nestable.js
bootkit/bootstrap-datepicker.min.js
bootkit/bootstrap-timepicker.js
bootkit/bootstrap-datepicker3.min.css
bootkit/bootstrap-timepicker.css
bootkit/init-datepicker.js
bootkit/init-timepicker.js
bootkit/jquery.nestable.css
bootkit/bootstrap.min.css
bootkit/font-awesome.min.css
bootkit/fullcalendar.min.css
bootkit/fullcalendar.print.min.css
bootkit/bootstrap.min.js
bootkit/fullcalendar.js
bootkit/init-calendar.js
bootkit/jquery-ui.min.js
bootkit/jquery.min.js
bootkit/modernizr-custom.js
bootkit/moment.js
bootkit/autosize.min.js
bootkit/update_tree.js
notification
)
