class CreateConsumables < ActiveRecord::Migration[5.1]
  def change
    create_table :consumables do |t|
      t.string :name

      t.timestamps
    end
  end
end
