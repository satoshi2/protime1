class CreateJobLinks < ActiveRecord::Migration[5.1]
  def change
    create_table :job_links do |t|
      t.integer :job_id
      t.integer :jobable_id
      t.string :jobable_type

      t.timestamps
    end
  end
end
