class CreateTaskPerformAssets < ActiveRecord::Migration[5.1]
  def change
    create_table :task_perform_assets do |t|
      t.string :kind
      t.integer :task_perform_id

      t.timestamps
    end
  end
end
