class ChangePersonalNumberTypeForWorker < ActiveRecord::Migration[5.1]
  def change
    change_column(:workers, :personal_number, :string)
  end
end
