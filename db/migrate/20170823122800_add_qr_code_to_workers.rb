class AddQrCodeToWorkers < ActiveRecord::Migration[5.1]
  def change
    add_column :workers, :qr_code, :string
  end
end
