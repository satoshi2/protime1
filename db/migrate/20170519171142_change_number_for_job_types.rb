class ChangeNumberForJobTypes < ActiveRecord::Migration[5.1]
  def change
    change_column(:job_types, :number, :string)
  end
end
