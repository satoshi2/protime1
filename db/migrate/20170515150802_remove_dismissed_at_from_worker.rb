class RemoveDismissedAtFromWorker < ActiveRecord::Migration[5.1]
  def change
    remove_column :workers, :dismissed_at, :string
  end
end
