class AddStatusToTaskPerforms < ActiveRecord::Migration[5.1]
  def change
    add_column :task_performs, :status, :string
  end
end
