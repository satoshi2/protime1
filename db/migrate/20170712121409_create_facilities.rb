class CreateFacilities < ActiveRecord::Migration[5.1]
  def change
    create_table :facilities do |t|
      t.string :name
      t.integer :division_id
      t.string :longtitude
      t.string :latitude

      t.timestamps
    end
  end
end
