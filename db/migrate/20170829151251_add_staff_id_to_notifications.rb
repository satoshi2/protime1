class AddStaffIdToNotifications < ActiveRecord::Migration[5.1]
  def change
    add_column :notifications, :staff_id, :integer
  end
end
