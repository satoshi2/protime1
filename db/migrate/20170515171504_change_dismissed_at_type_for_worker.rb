class ChangeDismissedAtTypeForWorker < ActiveRecord::Migration[5.1]
  def change
    change_column(:workers, :dismissed_at, :datetime , default: "31.12.9999")
  end
end
