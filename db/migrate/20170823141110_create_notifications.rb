class CreateNotifications < ActiveRecord::Migration[5.1]
  def change
    create_table :notifications do |t|
      t.string :title
      t.text :body
      t.integer :notificable_id
      t.string :notificable_type

      t.timestamps
    end
  end
end
