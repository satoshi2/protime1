class CreateVacations < ActiveRecord::Migration[5.1]
  def change
    create_table :vacations do |t|
      t.datetime :begin_at
      t.datetime :finish_at
      t.integer :worker_id

      t.timestamps
    end
  end
end
