class CreateWorkers < ActiveRecord::Migration[5.1]
  def change
    create_table :workers do |t|
      t.string :name
      t.string :surname
      t.string :middle_name
      t.integer :personal_number
      t.integer :contract_type_id
      t.string :profession_id
      t.datetime :birthday
      t.datetime :employed_at
      t.string :dismissed_at
      t.string :phone
      t.string :encrypted_password
      t.string :email
      t.integer :work_time_limit_id
      t.integer :time_tracking

      t.timestamps
    end
  end
end
