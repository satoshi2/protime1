class DefaultStatusTaskPerforms < ActiveRecord::Migration[5.1]
  def change
    change_column :task_performs, :status , :string, :default => "pending"
  end
end
