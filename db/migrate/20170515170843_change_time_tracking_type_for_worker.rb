class ChangeTimeTrackingTypeForWorker < ActiveRecord::Migration[5.1]
  def change
    change_column(:workers, :time_tracking, :string)
  end
end
