class RemoveCompanyIdFromDivision < ActiveRecord::Migration[5.1]
  def change
    remove_column :divisions, :company_id, :string
  end
end
