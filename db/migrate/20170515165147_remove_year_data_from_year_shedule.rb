class RemoveYearDataFromYearShedule < ActiveRecord::Migration[5.1]
  def change
    remove_column :year_shedules, :year, :string
    remove_column :year_shedules, :data, :string
  end
end
