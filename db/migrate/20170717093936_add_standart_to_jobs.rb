class AddStandartToJobs < ActiveRecord::Migration[5.1]
  def change
    add_column :jobs, :standart, :float
    change_column :jobs , :units, :float
  end
end
