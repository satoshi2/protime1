class AddApiTokenAndFirebaseToken < ActiveRecord::Migration[5.1]
  def change
    add_column :workers, :api_token, :string
    add_column :workers, :firebase_token, :string
  end
end
