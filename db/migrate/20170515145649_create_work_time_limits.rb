class CreateWorkTimeLimits < ActiveRecord::Migration[5.1]
  def change
    create_table :work_time_limits do |t|
      t.integer :hours

      t.timestamps
    end
  end
end
