class CreateYearShedules < ActiveRecord::Migration[5.1]
  def change
    create_table :year_shedules do |t|
      t.datetime :year
      t.datetime :data
      t.string :name

      t.timestamps
    end
  end
end
