class AddYearDataToYearShedule < ActiveRecord::Migration[5.1]
  def change
    add_column :year_shedules, :year, :integer
    add_column :year_shedules, :data, :text
  end
end
