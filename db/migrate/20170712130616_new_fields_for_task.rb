class NewFieldsForTask < ActiveRecord::Migration[5.1]
  def change
    add_column :tasks , :worker_id , :integer
    add_column :tasks , :difficult , :integer
    add_column :tasks , :description , :text
    add_column :tasks , :perform_conditions, :string
  end
end
