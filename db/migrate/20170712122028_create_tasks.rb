class CreateTasks < ActiveRecord::Migration[5.1]
  def change
    create_table :tasks do |t|
      t.string :number
      t.string :name
      t.integer :facility_id
      t.integer :scope_of_work
      t.datetime :begin_at
      t.datetime :finish_at
      t.string :cost
      t.integer :complexity

      t.timestamps
    end
  end
end
