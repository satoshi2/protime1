class AddDismissedAtToWorker < ActiveRecord::Migration[5.1]
  def change
    add_column :workers, :dismissed_at, :datetime
  end
end
