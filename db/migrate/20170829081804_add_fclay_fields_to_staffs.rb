class AddFclayFieldsToStaffs < ActiveRecord::Migration[5.1]
  def change

    add_column :staffs, :file_name, :string
    add_column :staffs, :file_size, :integer
    add_column :staffs, :file_status, :string, :default => "new"
    add_column :staffs, :file_location, :string
    add_column :staffs, :content_type, :string

  end


end
