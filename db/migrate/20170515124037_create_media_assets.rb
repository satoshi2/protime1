class CreateMediaAssets < ActiveRecord::Migration[5.1]
  def change
    create_table :media_assets do |t|
      t.string :url
      t.integer :mediaable_id
      t.string :mediaable_type
      t.string :kind

      t.timestamps
    end
  end
end
