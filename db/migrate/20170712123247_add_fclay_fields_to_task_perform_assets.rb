class AddFclayFieldsToTaskPerformAssets < ActiveRecord::Migration[5.1]
  def change

    add_column :task_perform_assets, :file_name, :string
    add_column :task_perform_assets, :file_size, :integer
    add_column :task_perform_assets, :file_status, :string, :default => "new"
    add_column :task_perform_assets, :file_location, :string
    add_column :task_perform_assets, :content_type, :string

  end


end
