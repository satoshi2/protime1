class CreateTaskJobLinks < ActiveRecord::Migration[5.1]
  def change
    create_table :task_job_links do |t|
      t.integer :job_id
      t.integer :task_id

      t.timestamps
    end
  end
end
