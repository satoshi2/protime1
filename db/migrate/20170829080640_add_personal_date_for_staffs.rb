class AddPersonalDateForStaffs < ActiveRecord::Migration[5.1]
  def change
    add_column :staffs, :surname , :string
    add_column :staffs, :patronymic, :string
    add_column :staffs, :phone_number, :string
    add_column :staffs, :position, :string
  end
end
