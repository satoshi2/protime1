class CreateJobs < ActiveRecord::Migration[5.1]
  def change
    create_table :jobs do |t|
      t.integer :job_type_id
      t.integer :job_unit_id
      t.text :desc
      t.string :work_time

      t.timestamps
    end
  end
end
