class AddParentColumnsToDivision < ActiveRecord::Migration[5.1]
  def change
    add_column :divisions, :parent_id, :integer
    add_column :divisions, :parent_type, :string
  end
end
