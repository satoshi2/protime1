class AddDetailsToTaskPerforms < ActiveRecord::Migration[5.1]
  def change
    add_column :task_performs, :details, :text
  end
end
