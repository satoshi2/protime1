class AddPriorityToDivisions < ActiveRecord::Migration[5.1]
  def change
    add_column :divisions, :priority, :integer
  end
end
