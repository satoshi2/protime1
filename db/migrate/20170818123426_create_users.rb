class CreateUsers < ActiveRecord::Migration[5.1]
  def change
    create_table :users do |t|
      t.string :phone_number
      t.string :api_token
      t.string :firebase_token

      t.timestamps
    end
  end
end
