class CreateTaskPerforms < ActiveRecord::Migration[5.1]
  def change
    create_table :task_performs do |t|
      t.integer :task_id
      t.datetime :begin_at
      t.datetime :finish_at
      t.text :worker_report
      t.text :auditor_report
      t.integer :auditor_id

      t.timestamps
    end
  end
end
