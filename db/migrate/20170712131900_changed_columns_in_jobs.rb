class ChangedColumnsInJobs < ActiveRecord::Migration[5.1]
  def change
    add_column :jobs , :name , :string
    add_column :jobs , :number , :string
    add_column :jobs , :units , :string
    remove_column :jobs , :job_type_id
    remove_column :jobs , :job_unit_id

  end
end
