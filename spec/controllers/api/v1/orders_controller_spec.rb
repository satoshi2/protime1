require 'rails_helper'

RSpec.describe Api::V1::OrdersController, type: :controller do

  include ApiHelper
  include AuthHelper

  before(:each) do
    request.headers["accept"] = 'application/json'
  end

  describe "GET index" do

    it "gives error_code 0", current_user: true do
      get :index
      response_json = JSON.parse response.body
      response_json["error_code"].should eq(0)
    end
    it "GET task ", current_user: true do
      task = create :task,{:worker_id => nil}
      get :index
      response_json = JSON.parse response.body
      response_json["error_code"].should eq(0)

      response_json["data"].first.keys.each do |f|
        %w(id number name scope_of_work cost complexity difficult description perform_conditions object_name job_type begin_at finish_at).include?(f).should eq(true)
      end
    end

  end

end
