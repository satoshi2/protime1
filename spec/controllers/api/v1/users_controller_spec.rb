require 'rails_helper'

RSpec.describe Api::V1::UsersController, type: :controller do

  include ApiHelper
  include AuthHelper


  before(:each) do
    request.headers["accept"] = 'application/json'
  end

  describe "POST login" do

    it "login with phone_number" do
      password = Faker::Internet.password
      phone = "8800555355"
      worker = create :worker, {:phone => phone ,:encrypted_password => password}
      post :login, params: {:user => {:phone_number => worker.phone, :password => password}}, :format => :json
      response.status.should eq(200)
      response_json = JSON.parse response.body
      response_json["error_code"].should eq(0)

    end

    it "login with phone_number get keys" do
      password = Faker::Internet.password
      phone = "8800555355"
      worker = create :worker, {:phone => phone ,:encrypted_password => password}
      post :login, params: {:user => {:phone_number => worker.phone, :password => password}}, :format => :json
      response.status.should eq(200)
      response_json = JSON.parse response.body
      response_json["error_code"].should eq(0)
      response_json["data"].keys.each do |f|
        %w(id phone fio api_token).include?(f).should eq(true)
      end
    end
  end
end
