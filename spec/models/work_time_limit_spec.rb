require 'rails_helper'

RSpec.describe WorkTimeLimit, type: :model do
  it "Not create instance" do
    a = WorkTimeLimit.new
    a.save
    a.persisted?.should eq(false)
  end

  it "Create instance" do
    a = create :work_time_limit
    a.persisted?.should eq(true)
  end
end
