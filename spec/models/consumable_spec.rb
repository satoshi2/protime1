require 'rails_helper'

RSpec.describe Consumable, type: :model do
  it "Not create instance" do
    a = Consumable.new
    a.save
    a.persisted?.should eq(false)
  end

  it "Create instance" do
    a = create :consumable
    a.persisted?.should eq(true)
  end
end
