require 'rails_helper'

RSpec.describe Inventory, type: :model do
  it "Not create instance" do
    a = Inventory.new
    a.save
    a.persisted?.should eq(false)
  end

  it "Create instance" do
    a = create :inventory
    a.persisted?.should eq(true)
  end
end
