require 'rails_helper'

RSpec.describe Profession, type: :model do
  it "Not create instance" do
    a = Profession.new
    a.save
    a.persisted?.should eq(false)
  end

  it "Create instance" do
    a = create :profession
    a.persisted?.should eq(true)
  end
end
