require 'rails_helper'

RSpec.describe Appliance, type: :model do

    it "Not create instance" do
      a = Appliance.new
      a.save
      a.persisted?.should eq(false)
    end

    it "Create instance" do
      a = create :appliance
      a.persisted?.should eq(true)
    end

end
