module AuthHelper

  include ApiHelper

  extend ActiveSupport::Concern

  included do

    before(:each) do
      request.headers["accept"] = 'application/json'
    end

    before(:each, current_user: true) do
      token = generate_token
      @current_user = create :worker, {:api_token => token[:md5]}
      request.headers["X-Api-Token"] = token[:raw]
    end

  end


end
