FactoryGirl.define do
  factory :worker do
    surname {Faker::Name.first_name}
    name {Faker::Name.first_name}
    middle_name {Faker::Name.first_name}
    contract_type {create :contract_type}
    profession {create :profession}
    work_time_limit {create :work_time_limit}
  end
end
