FactoryGirl.define do
  factory :notification do
    title "MyString"
    body "MyText"
    notificable_id 1
    notificable_type "MyString"
  end
end
