FactoryGirl.define do
  factory :task do
    number {Faker::Number.digit}
    name {Faker::Name.first_name}
    facility {create :facility}
    job {create :job}
    worker{create :worker}
    scope_of_work {Faker::Number.digit}
    begin_at {Faker::Date.between(3.days.ago,3.days.ago)}
    finish_at {Faker::Date.between(1.days.ago,1.days.ago)}
    cost {Faker::Number.digit}
    complexity {Faker::Number.digit}
  end
end
