FactoryGirl.define do
  factory :facility do
    name {Faker::Name.first_name }
    division {create :division}
    longtitude "MyString"
    latitude "MyString"
  end
end
