FactoryGirl.define do
  factory :user do
    phone_number "MyString"
    api_token "MyString"
    firebase_token "MyString"
  end
end
