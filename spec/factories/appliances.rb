FactoryGirl.define do
  factory :appliance do
    name {Faker::Name.first_name }
  end
end
