require 'test_helper'

class TaskJobLinksControllerTest < ActionDispatch::IntegrationTest
  setup do
    @task_job_link = task_job_links(:one)
  end

  test "should get index" do
    get task_job_links_url
    assert_response :success
  end

  test "should get new" do
    get new_task_job_link_url
    assert_response :success
  end

  test "should create task_job_link" do
    assert_difference('TaskJobLink.count') do
      post task_job_links_url, params: { task_job_link: { job_id: @task_job_link.job_id, task_id: @task_job_link.task_id } }
    end

    assert_redirected_to task_job_link_url(TaskJobLink.last)
  end

  test "should show task_job_link" do
    get task_job_link_url(@task_job_link)
    assert_response :success
  end

  test "should get edit" do
    get edit_task_job_link_url(@task_job_link)
    assert_response :success
  end

  test "should update task_job_link" do
    patch task_job_link_url(@task_job_link), params: { task_job_link: { job_id: @task_job_link.job_id, task_id: @task_job_link.task_id } }
    assert_redirected_to task_job_link_url(@task_job_link)
  end

  test "should destroy task_job_link" do
    assert_difference('TaskJobLink.count', -1) do
      delete task_job_link_url(@task_job_link)
    end

    assert_redirected_to task_job_links_url
  end
end
