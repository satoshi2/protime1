require 'test_helper'

class WorkersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @worker = workers(:one)
  end

  test "should get index" do
    get workers_url
    assert_response :success
  end

  test "should get new" do
    get new_worker_url
    assert_response :success
  end

  test "should create worker" do
    assert_difference('Worker.count') do
      post workers_url, params: { worker: { birthday: @worker.birthday, contract_type_id: @worker.contract_type_id, dismissed_at: @worker.dismissed_at, email: @worker.email, employed_at: @worker.employed_at, encrypted_password: @worker.encrypted_password, middle_name: @worker.middle_name, name: @worker.name, personal_number: @worker.personal_number, phone: @worker.phone, profession_id: @worker.profession_id, surname: @worker.surname, time_tracking: @worker.time_tracking, work_time_limit_id: @worker.work_time_limit_id } }
    end

    assert_redirected_to worker_url(Worker.last)
  end

  test "should show worker" do
    get worker_url(@worker)
    assert_response :success
  end

  test "should get edit" do
    get edit_worker_url(@worker)
    assert_response :success
  end

  test "should update worker" do
    patch worker_url(@worker), params: { worker: { birthday: @worker.birthday, contract_type_id: @worker.contract_type_id, dismissed_at: @worker.dismissed_at, email: @worker.email, employed_at: @worker.employed_at, encrypted_password: @worker.encrypted_password, middle_name: @worker.middle_name, name: @worker.name, personal_number: @worker.personal_number, phone: @worker.phone, profession_id: @worker.profession_id, surname: @worker.surname, time_tracking: @worker.time_tracking, work_time_limit_id: @worker.work_time_limit_id } }
    assert_redirected_to worker_url(@worker)
  end

  test "should destroy worker" do
    assert_difference('Worker.count', -1) do
      delete worker_url(@worker)
    end

    assert_redirected_to workers_url
  end
end
