require 'test_helper'

class JobUnitsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @job_unit = job_units(:one)
  end

  test "should get index" do
    get job_units_url
    assert_response :success
  end

  test "should get new" do
    get new_job_unit_url
    assert_response :success
  end

  test "should create job_unit" do
    assert_difference('JobUnit.count') do
      post job_units_url, params: { job_unit: { name: @job_unit.name } }
    end

    assert_redirected_to job_unit_url(JobUnit.last)
  end

  test "should show job_unit" do
    get job_unit_url(@job_unit)
    assert_response :success
  end

  test "should get edit" do
    get edit_job_unit_url(@job_unit)
    assert_response :success
  end

  test "should update job_unit" do
    patch job_unit_url(@job_unit), params: { job_unit: { name: @job_unit.name } }
    assert_redirected_to job_unit_url(@job_unit)
  end

  test "should destroy job_unit" do
    assert_difference('JobUnit.count', -1) do
      delete job_unit_url(@job_unit)
    end

    assert_redirected_to job_units_url
  end
end
