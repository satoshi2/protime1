require 'test_helper'

class WorkTimeLimitsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @work_time_limit = work_time_limits(:one)
  end

  test "should get index" do
    get work_time_limits_url
    assert_response :success
  end

  test "should get new" do
    get new_work_time_limit_url
    assert_response :success
  end

  test "should create work_time_limit" do
    assert_difference('WorkTimeLimit.count') do
      post work_time_limits_url, params: { work_time_limit: { hours: @work_time_limit.hours } }
    end

    assert_redirected_to work_time_limit_url(WorkTimeLimit.last)
  end

  test "should show work_time_limit" do
    get work_time_limit_url(@work_time_limit)
    assert_response :success
  end

  test "should get edit" do
    get edit_work_time_limit_url(@work_time_limit)
    assert_response :success
  end

  test "should update work_time_limit" do
    patch work_time_limit_url(@work_time_limit), params: { work_time_limit: { hours: @work_time_limit.hours } }
    assert_redirected_to work_time_limit_url(@work_time_limit)
  end

  test "should destroy work_time_limit" do
    assert_difference('WorkTimeLimit.count', -1) do
      delete work_time_limit_url(@work_time_limit)
    end

    assert_redirected_to work_time_limits_url
  end
end
