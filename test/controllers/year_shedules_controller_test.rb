require 'test_helper'

class YearShedulesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @year_shedule = year_shedules(:one)
  end

  test "should get index" do
    get year_shedules_url
    assert_response :success
  end

  test "should get new" do
    get new_year_shedule_url
    assert_response :success
  end

  test "should create year_shedule" do
    assert_difference('YearShedule.count') do
      post year_shedules_url, params: { year_shedule: { data: @year_shedule.data, name: @year_shedule.name, year: @year_shedule.year } }
    end

    assert_redirected_to year_shedule_url(YearShedule.last)
  end

  test "should show year_shedule" do
    get year_shedule_url(@year_shedule)
    assert_response :success
  end

  test "should get edit" do
    get edit_year_shedule_url(@year_shedule)
    assert_response :success
  end

  test "should update year_shedule" do
    patch year_shedule_url(@year_shedule), params: { year_shedule: { data: @year_shedule.data, name: @year_shedule.name, year: @year_shedule.year } }
    assert_redirected_to year_shedule_url(@year_shedule)
  end

  test "should destroy year_shedule" do
    assert_difference('YearShedule.count', -1) do
      delete year_shedule_url(@year_shedule)
    end

    assert_redirected_to year_shedules_url
  end
end
