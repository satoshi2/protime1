require 'test_helper'

class JobLinksControllerTest < ActionDispatch::IntegrationTest
  setup do
    @job_link = job_links(:one)
  end

  test "should get index" do
    get job_links_url
    assert_response :success
  end

  test "should get new" do
    get new_job_link_url
    assert_response :success
  end

  test "should create job_link" do
    assert_difference('JobLink.count') do
      post job_links_url, params: { job_link: { job_id: @job_link.job_id, jobable_id: @job_link.jobable_id, jobable_type: @job_link.jobable_type } }
    end

    assert_redirected_to job_link_url(JobLink.last)
  end

  test "should show job_link" do
    get job_link_url(@job_link)
    assert_response :success
  end

  test "should get edit" do
    get edit_job_link_url(@job_link)
    assert_response :success
  end

  test "should update job_link" do
    patch job_link_url(@job_link), params: { job_link: { job_id: @job_link.job_id, jobable_id: @job_link.jobable_id, jobable_type: @job_link.jobable_type } }
    assert_redirected_to job_link_url(@job_link)
  end

  test "should destroy job_link" do
    assert_difference('JobLink.count', -1) do
      delete job_link_url(@job_link)
    end

    assert_redirected_to job_links_url
  end
end
