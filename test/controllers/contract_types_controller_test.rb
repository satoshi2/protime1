require 'test_helper'

class ContracttypesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @contract_type = contracttypes(:one)
  end

  test "should get index" do
    get contracttypes_url
    assert_response :success
  end

  test "should get new" do
    get new_contracttype_url
    assert_response :success
  end

  test "should create contracttype" do
    assert_difference('Contracttype.count') do
      post contracttypes_url, params: { contracttype: { name: @contract_type.name } }
    end

    assert_redirected_to contracttype_url(Contracttype.last)
  end

  test "should show contracttype" do
    get contracttype_url(@contract_type)
    assert_response :success
  end

  test "should get edit" do
    get edit_contracttype_url(@contract_type)
    assert_response :success
  end

  test "should update contracttype" do
    patch contracttype_url(@contracttype), params: { contracttype: { name: @contract_type.name } }
    assert_redirected_to contracttype_url(@contract_type)
  end

  test "should destroy contracttype" do
    assert_difference('Contracttype.count', -1) do
      delete contracttype_url(@contract_type)
    end

    assert_redirected_to contracttypes_url
  end
end
