require 'test_helper'

class TaskPerformsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @task_perform = task_performs(:one)
  end

  test "should get index" do
    get task_performs_url
    assert_response :success
  end

  test "should get new" do
    get new_task_perform_url
    assert_response :success
  end

  test "should create task_perform" do
    assert_difference('TaskPerform.count') do
      post task_performs_url, params: { task_perform: { auditor_id: @task_perform.auditor_id, auditor_report: @task_perform.auditor_report, begin_at: @task_perform.begin_at, finish_at: @task_perform.finish_at, task_id: @task_perform.task_id, worker_report: @task_perform.worker_report } }
    end

    assert_redirected_to task_perform_url(TaskPerform.last)
  end

  test "should show task_perform" do
    get task_perform_url(@task_perform)
    assert_response :success
  end

  test "should get edit" do
    get edit_task_perform_url(@task_perform)
    assert_response :success
  end

  test "should update task_perform" do
    patch task_perform_url(@task_perform), params: { task_perform: { auditor_id: @task_perform.auditor_id, auditor_report: @task_perform.auditor_report, begin_at: @task_perform.begin_at, finish_at: @task_perform.finish_at, task_id: @task_perform.task_id, worker_report: @task_perform.worker_report } }
    assert_redirected_to task_perform_url(@task_perform)
  end

  test "should destroy task_perform" do
    assert_difference('TaskPerform.count', -1) do
      delete task_perform_url(@task_perform)
    end

    assert_redirected_to task_performs_url
  end
end
