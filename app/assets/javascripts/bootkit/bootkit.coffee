#= require rails-ujs
#= require jquery
#= require bootstrap-sprockets
#= require bootkit/jquery.nicescroll.min
#= require bootkit/autosize.min
#= require bootkit/main
#= require moment
#= require fullcalendar
#= require bootkit/locale/ru

$(document).ready ->
  $('#calendar').fullCalendar
    locale: "ru"
    eventSources: [ {
      lang:"ru"
      url: '/tasks.json'
      color: '#4aa9e9'
      textColor: 'white'
  } ]

  return

$(document).ready ->
  $('ul li a').click ->
    console.log("1")
    $('li a').removeClass 'active'
    $(this).addClass 'active'
    return
  return
