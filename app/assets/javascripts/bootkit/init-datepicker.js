(function($) {

    'use strict';

    $(document).ready(function() {

        $('.js-datepicker').datepicker({
            autoclose: true,
            format: "dd/mm/yyyy"
        });

    });

})(window.jQuery);
