processFields = (e) ->
  if $("#notification_kind").val() == 'Персональное'
    $("#notification_user").show()
  else
    $("#notification_user").hide()

$ ->
  processFields()
  $("#notification_kind").change processFields
