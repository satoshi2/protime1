class TaskPerform < ApplicationRecord
  validates_presence_of :task_id
  belongs_to :task
  has_many :task_perform_assets
  attr_writer :assets
  after_update :fetch_status


  def fetch_status
    errors = []
    self.task.perform_conditions.split(',').each do |f|
      case f
        when "start_finish"
          terror = Hash.new
          terror['key'] = "start_finish"
          terror['desc'] = "Need close the task"
          errors.push terror unless self.finish_at
        when "plan"

        when "qr_code"
          terror = Hash.new
          terror['key'] = "qr_code"
          terror['desc'] = "Need qr_code asset"
          errors.push terror unless task_perform_assets.where(:kind => "qr_code").present?
        when "after_last"

        when "selfie"
          terror = Hash.new
          terror['key'] = "selfie"
          terror['desc'] = "Need selfie asset"
          errors.push terror unless task_perform_assets.where(:kind => "selfie").present?
        when "room_before"
          terror = Hash.new
          terror['key'] = "room_before"
          terror['desc'] = "Need room_before asset"
          errors.push terror unless task_perform_assets.where(:kind => "room_before").present?
        when "room_after"
          terror = Hash.new
          terror['key'] = "room_after"
          terror['desc'] = "Need room_after asset"
          errors.push terror unless task_perform_assets.where(:kind => "room_after").present?
        when "worker_report"
          terror = Hash.new
          terror['key'] = "worker_report"
          terror['desc'] = "Need worker_report"
          errors.push terror unless self.worker_report
        when "auditor_report"
          terror = Hash.new
          terror['key'] = "auditor_report"
          terror['desc'] = "Need auditor_report"
          errors.push terror unless self.auditor_report && auditor_id
      end
    end

    self.update_attribute(:status, "successful") if errors.empty?
    errors

  end

end
