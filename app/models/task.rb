class Task < ApplicationRecord
  has_many :job_links
  belongs_to :facility
  belongs_to :job
  belongs_to :worker, optional: true
  has_one :task_perform
  has_many :notifications
  validates_presence_of :name , :number
  attr_accessor :status
  attr_accessor :night_hours
  attr_accessor :control
  attr_accessor :begin_at_date
  attr_accessor :begin_at_time
  attr_accessor :finish_at_date
  attr_accessor :finish_at_time
  COMPLEXITY = %w(1 2 3)
  validate :date_validation
  before_create :fetch_perfrom_conditions
  before_validation :set_begin_at
  before_validation :set_finish_at


  def set_begin_at
    date = @begin_at_date+" "+@begin_at_time if @begin_at_date && @begin_at_time
    self.begin_at = DateTime.parse(date) if @begin_at_date && @begin_at_time
  end

  def set_finish_at
    date = @finish_at_date+" "+@finish_at_time  if @finish_at_date && @finish_at_time
    self.finish_at = DateTime.parse(date) if @finish_at_date && @finish_at_time
  end

  def date_validation
    if self.finish_at.to_i < self.begin_at.to_i
      errors.add :finish_at, "cannot be less than start date "
    end
  end

  def status
    return "free" unless self.worker.present?
    return "sheduled" unless self.task_perform.present?
    case self.task_perform.status
      when "pending"
        "inprogress"
      when "successful"
        "completed"
      else
        "unknown"
    end
  end

  def fetch_status
    return unless task_perform.present?
    task_perform.fetch_status
  end

  def fetch_perfrom_conditions
    self.perform_conditions = @control.join(",") if @control
  end

end
