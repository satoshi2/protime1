class TaskJobLink < ApplicationRecord
  belongs_to :job
  belongs_to :task

  validates_presence_of :job_id , :task_id
end
