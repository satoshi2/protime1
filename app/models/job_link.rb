class JobLink < ApplicationRecord
  belongs_to :job
  belongs_to :jobable, polymorphic: true
  validates_presence_of :job, :jobable
  validate :check_uniqness
  def check_uniqness
    self.errors.add(:job_link, "already exist") if self.class.where(:job_id => self.job_id).where(:jobable_type => self.jobable_type).where(:jobable_id=> self.jobable_id).present?
  end

end
