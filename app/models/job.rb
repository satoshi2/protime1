class Job < ApplicationRecord
  validates_presence_of :name ,:work_time , :number , :units
  WORK_TIME = ["да" , "нет" , "обед"]
  has_many :tasks
  has_many :job_links
  def inventories
    job_links.where(:jobable_type => "Inventory").map{|a| a.jobable}
  end
  def appliances
    job_links.where(:jobable_type => "Appliance").map{|a| a.jobable}
  end
  def consumables
    job_links.where(:jobable_type => "Consumable").map{|a| a.jobable}
  end

end
