class Appliance < ApplicationRecord
  has_many :job_links, as: :jobable , dependent: :destroy
  has_many :jobs, through: :job_links
  validates_presence_of :name
end
