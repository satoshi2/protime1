class Worker < ApplicationRecord
  belongs_to :contract_type
  belongs_to :profession
  belongs_to :work_time_limit
  validates_presence_of :name , :surname , :middle_name
  before_create :generate_qr_code
  TIME_TRACK = ["В пределах плана", "По плану" , "По факту"]


  def generate_qr_code
    self.qr_code = SecureRandom.hex 15
  end
end
