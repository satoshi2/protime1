class ContractType < ApplicationRecord
  validates_presence_of :name
  has_many :workers
end
