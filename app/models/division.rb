class Division < ApplicationRecord
  validates_presence_of  :name
  belongs_to :parent, polymorphic: true, optional: true
  has_many :divisions , as: :parent
  has_many :facilities

end
