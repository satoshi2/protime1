class Notification < ApplicationRecord
  belongs_to :notificable, polymorphic: true, required: false
  belongs_to :task
  belongs_to :staff
  validates_presence_of :title, :body
  attr_accessor :kind

end
