class WorkTimeLimit < ApplicationRecord
  validates_presence_of :hours
  has_many :workers
end
