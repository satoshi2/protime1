class Facility < ApplicationRecord
  belongs_to :division
  validates_presence_of :name
end
