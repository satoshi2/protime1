class TaskPerformAsset < ApplicationRecord

  has_fclay_attachment
  belongs_to :task_perform
  validates_presence_of :kind , :task_perform
  after_create :call_task_perform_fetch_status

  def call_task_perform_fetch_status
    return unless task_perform.present?
    task_perform.fetch_status
  end

end
