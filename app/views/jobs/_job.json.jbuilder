json.extract! job, :id, :job_type_id, :job_unit_id, :desc, :work_time, :created_at, :updated_at
json.url job_url(job, format: :json)
