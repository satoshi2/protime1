json.extract! appliance, :id, :name, :created_at, :updated_at
json.url appliance_url(appliance, format: :json)
