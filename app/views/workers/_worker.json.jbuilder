json.extract! worker, :id, :name, :surname, :middle_name, :personal_number, :contract_type_id, :profession_id, :birthday, :employed_at, :dismissed_at, :phone, :encrypted_password, :email, :work_time_limit_id, :time_tracking, :created_at, :updated_at
json.url worker_url(worker, format: :json)
