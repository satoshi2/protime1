json.extract! task_job_link, :id, :job_id, :task_id, :created_at, :updated_at
json.url task_job_link_url(task_job_link, format: :json)
