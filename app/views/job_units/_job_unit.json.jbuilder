json.extract! job_unit, :id, :name, :created_at, :updated_at
json.url job_unit_url(job_unit, format: :json)
