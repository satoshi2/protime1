node(:error_code) {0}
child @task => :data do
  attribute :id, :number, :name, :scope_of_work, :cost, :complexity
  node(:object_name){|a| a.facility.name}
  node(:job_type){|a| a.job.name}
  node(:begin_at){|a| a.begin_at.to_i}
  node(:finish_at){|a| a.finish_at.to_i}
end
