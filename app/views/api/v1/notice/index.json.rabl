node(:error_code) {0}
child @notifications => :data do
  attribute :id, :title, :body, :task_id
  node(:photo){|a| a.staff.try(:file_url)}
  node(:fio){|a|  (a.staff.try(:surname)+" "+a.staff.try(:name)+" "+a.staff.try(:patronymic))}
  node(:phone){|a|  a.staff.try(:phone_number)}
  node(:position){|a|  a.staff.try(:position)}
  node(:created_at){|a| a.created_at.to_i}
end
