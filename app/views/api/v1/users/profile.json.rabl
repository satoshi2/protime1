node(:error_code) {0}
object false
child @user_to_render => :data do
  attributes :phone
  node(:fio){|a| (a.surname+" "+a.name+" "+a.middle_name)}
  if @api_token_to_render.present?
    node(:api_token) {@api_token_to_render}
  end
end
