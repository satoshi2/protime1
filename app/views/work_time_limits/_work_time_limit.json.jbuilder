json.extract! work_time_limit, :id, :hours, :created_at, :updated_at
json.url work_time_limit_url(work_time_limit, format: :json)
