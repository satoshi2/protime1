json.extract! media_asset, :id, :url, :mediaable_id, :mediaable_type, :kind, :created_at, :updated_at
json.url media_asset_url(media_asset, format: :json)
