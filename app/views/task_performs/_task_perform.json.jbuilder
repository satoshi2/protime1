json.extract! task_perform, :id, :task_id, :begin_at, :finish_at, :worker_report, :auditor_report, :auditor_id, :created_at, :updated_at
json.url task_perform_url(task_perform, format: :json)
