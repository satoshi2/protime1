json.extract! job_link, :id, :job_id, :jobable_id, :jobable_type, :created_at, :updated_at
json.url job_link_url(job_link, format: :json)
