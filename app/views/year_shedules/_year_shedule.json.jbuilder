json.extract! year_shedule, :id, :year, :data, :name, :created_at, :updated_at
json.url year_shedule_url(year_shedule, format: :json)
