json.extract! vacation, :id, :begin_at, :finish_at, :worker_id, :created_at, :updated_at
json.url vacation_url(vacation, format: :json)
