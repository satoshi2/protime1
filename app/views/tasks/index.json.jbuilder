json.array!(@tasks) do |task|
  json.extract! task, :id
  json.title task.name
  json.start task.begin_at
  json.end task.finish_at
  json.url task_url(task, format: :html)
end
