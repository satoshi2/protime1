module ApiHelper
  
  def fetch_actual_date 
    
    lasts_updated_at = []
    
    [Level, Lesson, LessonPart, Verb, Sentence].each do |f|
      lasts_updated_at.push f.maximum(:updated_at)
    end
    
    lasts_updated_at.compact!
    
    lasts_updated_at.size == 0 ? Time.now : lasts_updated_at.max
    
  end
  
  def generate_token size=25
      raw = SecureRandom.hex size
      md5 = Digest::MD5.hexdigest(raw)
      {:raw => raw, :md5 => md5}
  end
  
  def md5 raw
     Digest::MD5.hexdigest(raw)
  end
  
end