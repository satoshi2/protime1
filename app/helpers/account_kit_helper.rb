module AccountKitHelper

  def self.fetch_phone_number access_token

    begin
      response = HTTParty.get("https://graph.accountkit.com/v1.2/me/?access_token=#{access_token}")
      data = JSON.parse response.body
      data["phone"]["number"]

    rescue Exception => e
      Rails.logger.debug("AccountKitHelper Exception: #{e.message}")
      return nil
    end

  end
end
