module ApplicationHelper


  def fetch_li_status key
    params[:controller].to_sym == key ? 'active' : ""
  end

end
