class FacilitiesController < InheritedResources::Base
  skip_before_action :verify_authenticity_token
  before_action :authenticate_staff!

  layout "bootkit"

  private

  def facility_params
    params.require(:facility).permit(:name, :division_id, :longtitude ,:latitude)
  end
end
