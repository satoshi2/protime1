class DivisionsController < InheritedResources::Base
  skip_before_action :verify_authenticity_token
  before_action :authenticate_staff!
  layout "bootkit"
  def new
    @division = Division.new
  end
  def create
    resource = Division.new
    resource.name = params[:division][:name]
    resource.parent_id = params[:division][:parent_id]
    resource.parent_type = params[:division][:parent_type]
    resource.save
    redirect_to resource.parent
  end

  def update_tree

    tree_data = JSON.parse params[:tree_data]
    process_tree_data_array tree_data, true

    redirect_to company_path(params[:parent])

  end

  private

  def process_tree_data_array array, root=false

    priority = 0
    array.each do |f|
      division = Division.find(f["id"])
      division.update_attribute(:priority,priority)
      priority += 1
      division.update_attributes(:parent_id => params[:parent],:parent_type => "Company") if root
      childrens = f["children"]
      child_sections = []
      if childrens
        Division.where(:id => childrens.map{|a| a["id"]}).update_all({:parent_id => f["id"] , :parent_type => "Division"})
        process_tree_data_array(childrens)
      end
    end
  end

  def division_params
    params.require(:division).permit(:name)
  end
end
