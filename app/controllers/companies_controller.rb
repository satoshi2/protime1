class CompaniesController < InheritedResources::Base
  skip_before_action :verify_authenticity_token
  before_action :authenticate_staff!
  before_action :set_additional, :only => :show

  layout "bootkit"


  def reorder

  end
  private
    # Use callbacks to share common setup or constraints between actions.
    def set_company
      @company = Company.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def company_params
      params.require(:company).permit(:name)
    end

  private

    def set_additional
      @additional_js = %w(bootkit/jquery.nestable bootkit/init-nestable bootkit/update_tree)
      @additional_css = %w(bootkit/jquery.nestable)
    end

    def company_params
      params.require(:company).permit(:name)
    end
end
