class VacationsController < InheritedResources::Base
  skip_before_action :verify_authenticity_token
  before_action :authenticate_staff!
  layout "bootkit"
  before_action :set_additional, :only => [:edit,:new,:create]


  def set_additional
    @additional_js = %w(bootkit/bootstrap-datepicker.min bootkit/init-datepicker bootkit/init-timepicker bootkit/bootstrap-timepicker)
    @additional_css = %w(bootkit/bootstrap-datepicker3.min bootkit/bootstrap-timepicker)
  end
  private

  def vacation_params
    params.require(:vacation).permit(:begin_at, :finish_at, :worker_id)
  end
end
