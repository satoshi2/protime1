class ProfessionsController < InheritedResources::Base

  skip_before_action :verify_authenticity_token
  before_action :authenticate_staff!
  layout "bootkit"


  private

    def profession_params
      params.require(:profession).permit(:name)
    end
end
