class ConsumablesController < InheritedResources::Base
  skip_before_action :verify_authenticity_token
  before_action :authenticate_staff!
  layout "bootkit"
  private

    def consumable_params
      params.require(:consumable).permit(:name)
    end
end
