module Api
  module V1
    class UsersController < Api::ApiController

      before_action :require_user, :except => [:login]

      def login
        # return restrict_access 1,"Missed qr_code and phone_number" if !params[:user][:qr_code].present? && !params[:user][:phone_number].present?

        if params[:user][:phone_number].present?
          user = Worker.where(:phone => params[:user][:phone_number]).first
        elsif params[:user][:qr_code].present?
          user = Worker.where(:qr_code => params[:user][:qr_code]).first
        end

        return restrict_access(1, "Missed or incorrect qr_code and phone_number ") unless user

        if params[:user][:password].present?
          return restrict_access(1, "phone_number or password incorrect") unless user.encrypted_password == params[:user][:password]
          @current_user = user
          update_api_token user
        elsif params[:user][:qr_code].present?
          return restrict_access(1, "qr_code incorrect") unless user.qr_code == params[:user][:qr_code]
          @current_user = user
          update_api_token user
        else
          restrict_access(1, "Login or password incorrect")
        end

      end

      def update
        @user_to_render = @current_user
        begin
          @user_to_render.update_attributes(user_params) if params[:user].present?
          render :profile
        rescue Exception => e
          Rails.logger.error "Error updating:" + e.message
        end
      end

      private

      def update_api_token user
        token = generate_token
        user.update_attribute(:api_token,token[:md5])
        @user_to_render = user
        @api_token_to_render = token[:raw]
        render :profile
      end

      # def user_params_present?
      #   params[:user].present? && params[:user][:email].present? && params[:user][:password].present?
      # end

      def user_params
        params.require(:user).permit(:firebase_token)
      end


    end
  end
end
