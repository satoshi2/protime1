module Api
  module V1
    class OrdersController < Api::ApiController

      before_action :require_user

      def index
        if params[:date].present?
          actual_date = Time.at(params[:date].to_i)
          @user_tasks = Task.where(:worker_id => nil).where(:begin_at => actual_date)
        else
          @user_tasks = Task.where(:worker_id => nil)
        end
      end

      def show
        return restrict_access(1, "Can't find this task") unless Task.where(:id => params[:id]).present?
        control = Task.find(params[:id])
        if control.worker.present?
          render :json => {:error_code => 0, :error_message => "This order is already assigned to another user"}
        else
          @task = control
        end
      end

      def direction
        return restrict_access(1, "Missing status or order_id") unless params[:status].present? && params[:order_id].present?
        return restrict_access(1, "Can't find this order") unless Task.where(:id => params[:order_id]).present?
        control = Task.find(params[:order_id])
        return restrict_access(0, "This order is already assigned to another user") unless !control.worker
        if params[:status] == "0"
          render :json => {:error_code => 0, :error_message => "Your refusal to accept"}
        elsif params[:status] == "1"
          control.worker_id = @current_user.id
          if control.save
            render :json => {:error_code => 0, :message => "Congrats, this order is now yours"}
          else
            render :json => {:error_code => 1, :message => "Sorry, having problems with your appointment on the task"}
          end
        else
          render :json => {:error_code => 1, :message => "Sorry, unacceptable value for the status"}
        end
      end

    end
  end
end
