module Api
  module V1
    class NoticeController < Api::ApiController

      before_action :require_user

      def index
        total = Notification.where(:notificable_id => nil ).where(:notificable_type => nil)
        user = Notification.where(:notificable_id => @current_user.id).where(:notificable_type => "Worker")
        @notifications = total+user
      end


    end
  end
end
