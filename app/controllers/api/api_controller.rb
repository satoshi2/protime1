# -*- encoding : utf-8 -*-
module Api
    class ApiController < ApplicationController

      include ApplicationHelper

      skip_before_action :verify_authenticity_token

      def home
        render :text => "See API specification"
      end

      def generate_token size=25
        raw = SecureRandom.hex size
        md5 = Digest::MD5.hexdigest(raw)
        {:raw => raw, :md5 => md5}
      end

      def md5 raw
         Digest::MD5.hexdigest(raw)
      end

      def require_user
        require_api_token
        return unless @api_token
        @current_user = Worker.where(:api_token => md5(@api_token)).first
        if @current_user
          @current_user.update_attribute(:updated_at,Time.now)
        else
          restrict_access 1,"Api token is invalid"
        end
      end

      def require_api_token
        @api_token = request.headers["X-Api-Token"]
        restrict_access 5, "Missed api token" unless @api_token
      end

      def restrict_access error_code=999, message="Unknown error", http_code=400
        return if @restricted
        @restricted = true
        warden.try(:custom_failure!)
        render_error_code error_code, message, http_code
      end

      def render_error_code error_code=999, message="Unknown error", http_code=400
        render :json => {:error_code => error_code, :error_message => message}, :status => http_code
      end

    end
end
