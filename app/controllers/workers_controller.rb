class WorkersController < InheritedResources::Base
  skip_before_action :verify_authenticity_token
  before_action :authenticate_staff!
  layout "bootkit"
  before_action :set_additional, :only => [:edit,:new,:create]
  require 'rqrcode'
  
  def show
    @worker = Worker.find(params[:id])
    @qr_code = RQRCode::QRCode.new( "#{@worker.try(:qr_code)}", :size => 4, :level => :h )
  end

  def set_additional
    @additional_js = %w(bootkit/bootstrap-datepicker.min bootkit/init-datepicker bootkit/init-timepicker bootkit/bootstrap-timepicker)
    @additional_css = %w(bootkit/bootstrap-datepicker3.min bootkit/bootstrap-timepicker)
  end

  private

  def worker_params
    params.require(:worker).permit(:name, :surname, :middle_name, :personal_number,:qr_code, :contract_type_id, :profession_id, :birthday, :employed_at, :dismissed_at, :phone, :encrypted_password, :email, :work_time_limit_id, :time_tracking)
  end
end
