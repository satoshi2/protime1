class CalendarController < InheritedResources::Base
  skip_before_action :verify_authenticity_token
  layout "bootkit"
  before_action :set_additional
  def index

  end

  private

  def set_additional
    # @additional_js = %w(bootkit/bootstrap-datepicker.min bootkit/init-datepicker bootkit/init-timepicker bootkit/bootstrap-timepicker)
    @additional_css = %w(bootkit/fullcalendar.min )
  end


end
