class TaskJobLinksController < InheritedResources::Base
  skip_before_action :verify_authenticity_token
  before_action :authenticate_staff!
  layout "bootkit"

private

  def task_job_link_params
    params.require(:task_job_link).permit(:job_id, :task_id)
  end
end
