class Staffs::StaffController < InheritedResources::Base
  skip_before_action :verify_authenticity_token
  before_action :authenticate_staff!
  layout "bootkit"

  def show
    @staff = Staff.find(params[:id])
  end

  def edit
    @staff = Staff.find(params[:id])
  end

  def update
    resource = Staff.find(params[:id])
    resource.update(permit_params)
    resource.save
    redirect_to resource
  end


  private

  def permit_params
    params.require(:staff).permit(:name , :surname , :patronymic, :phone_number, :email, :position, :file)
  end
end
