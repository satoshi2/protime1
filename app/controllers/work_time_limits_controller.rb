class WorkTimeLimitsController < InheritedResources::Base
  skip_before_action :verify_authenticity_token
  before_action :authenticate_staff!
  layout "bootkit"
  private

    def work_time_limit_params
      params.require(:work_time_limit).permit(:hours)
    end
end
