class JobUnitsController < InheritedResources::Base
  skip_before_action :verify_authenticity_token
  before_action :authenticate_staff!
  layout "bootkit"
  private

    def job_unit_params
      params.require(:job_unit).permit(:name)
    end
end
