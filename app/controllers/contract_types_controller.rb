class ContractTypesController < InheritedResources::Base
  skip_before_action :verify_authenticity_token
  before_action :authenticate_staff!
  layout "bootkit"
  private
    # Use callbacks to share common setup or constraints between actions.
    def set_contract_type
      @contract_type = ContractType.find(params[:id])
    end

  def contract_type_params
    params.require(:contract_type).permit(:name)
  end
end
