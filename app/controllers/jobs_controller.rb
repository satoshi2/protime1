class JobsController < InheritedResources::Base
  skip_before_action :verify_authenticity_token
  before_action :authenticate_staff!

  layout "bootkit"
  private

    def job_params
      params.require(:job).permit(:name, :number, :units, :desc, :work_time)
    end
end
