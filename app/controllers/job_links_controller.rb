class JobLinksController < InheritedResources::Base
  skip_before_action :verify_authenticity_token
  before_action :authenticate_staff!
  layout "bootkit"
    def create
      create! do |success, failure|
        failure.html { redirect_to resource.job, :notice => resource.errors.full_messages.to_s }
        success.html { redirect_to resource.job }
      end
    end

    def show
      super do |success,failure|
        success.html { redirect_to resource.job }
      end
    end

    def destroy
      super do |success,failure|
        success.html { redirect_to resource.job }
      end
    end

  private

    def job_link_params
      params.require(:job_link).permit(:job_id, :jobable_id, :jobable_type)
    end
end
