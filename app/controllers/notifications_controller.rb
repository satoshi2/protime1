class NotificationsController < InheritedResources::Base
  skip_before_action :verify_authenticity_token
  before_action :authenticate_staff!
  before_action :set_additional, :only => [:edit,:new,:create]
  layout "bootkit"

  def create
    resource = Notification.new
    resource.title = params[:notification][:title]
    resource.body = params[:notification][:body]
    resource.task_id = params[:notification][:task_id]
    resource.staff_id = current_staff.id
    if params[:notification][:notificable].present?
      resource.notificable_id = params[:notification][:notificable]
      resource.notificable_type = "Worker"
    end
    resource.save
    redirect_to resource
  end



  private
    def set_additional
      @additional_js = %w(notification)
    end

    def notification_params
      params.require(:notification).permit(:title , :body , :task_id)
    end
end
