class TasksController < InheritedResources::Base
  skip_before_action :verify_authenticity_token
  before_action :authenticate_staff!
  before_action :set_additional, :only => [:edit,:new,:create]
  layout "bootkit"


  def index
    @tasks = Task.all
    respond_to do |format|
      format.html
      format.json
    end
  end
  private

  def set_additional
    @additional_js = %w(bootkit/bootstrap-datepicker.min bootkit/init-datepicker bootkit/init-timepicker bootkit/bootstrap-timepicker)
    @additional_css = %w(bootkit/bootstrap-datepicker3.min bootkit/bootstrap-timepicker)
  end

  def task_params
    params.require(:task).permit(:name, :number,:begin_at_date ,:begin_at_time,:finish_at_date,:finish_at_time, :cost, :worker_id, :job_id, :scope_of_work, :facility_id, :cost, :complexity, :control => [])
  end
end
