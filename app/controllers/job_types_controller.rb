class JobTypesController < InheritedResources::Base
  skip_before_action :verify_authenticity_token
  before_action :authenticate_staff!
  layout "bootkit"
  private

    def job_type_params
      params.require(:job_type).permit(:name, :number)
    end
end
