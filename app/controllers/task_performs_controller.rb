class TaskPerformsController < InheritedResources::Base
  skip_before_action :verify_authenticity_token
  before_action :authenticate_staff!
  layout "bootkit"
private

  def task_perform_params
    params.require(:task_perform).permit(:task_id, :begin_at, :finish_at, :worker_report, :auditor_report, :auditor_id)
  end
end
