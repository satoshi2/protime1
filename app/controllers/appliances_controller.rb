class AppliancesController < InheritedResources::Base
  skip_before_action :verify_authenticity_token
  before_action :authenticate_staff!
  layout "bootkit"
  private

    def appliance_params
      params.require(:appliance).permit(:name)
    end
end
