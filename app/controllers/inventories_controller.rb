class InventoriesController < InheritedResources::Base
  skip_before_action :verify_authenticity_token
  before_action :authenticate_staff!
  layout "bootkit"
  private

    def inventory_params
      params.require(:inventory).permit(:name)
    end
end
