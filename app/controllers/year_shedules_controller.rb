class YearShedulesController < InheritedResources::Base
  skip_before_action :verify_authenticity_token
  before_action :authenticate_staff!
  layout "bootkit"
  private

    def year_shedule_params
      params.require(:year_shedule).permit(:year, :name)
    end
end
