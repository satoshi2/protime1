class MediaAssetsController < InheritedResources::Base
  before_action :authenticate_staff!

  layout "bootkit"
private

  def media_asset_params
    params.require(:media_asset).permit(:url, :mediaable_id, :mediaable_type, :kind)
  end
end
